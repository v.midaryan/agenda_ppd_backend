const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const nodemailer = require("nodemailer");
const key = require('../../config/keys').secret;
const User = require('../../model/User');
const Salle = require('../../model/Salle');

/**
 * @route POST api/users/register
 * @desc Register the User
 * @access Public
 */
router.post('/register', (req, res) => {
    let {
        email,
        password,
        confirm_password,
        admin,
        company,
        job,
        name,
        surname,
        invite
    } = req.body;
    if (password !== confirm_password) {
        return res.status(500).json({
            msg: "les mots de passe ne correspondent pas"
        });
    }

    // Check for the Unique Email
    User.findOne({
        email: email
    }).then(user => {
        if (user) {
            return res.status(500).send("cette adresse email est déjà enregistrée !");
        } else {
            // The data is valid and new we can register the user
            let newUser = new User({
                password,
                email,
                admin,
                company,
                job,
                name,
                surname,
                invite
            });
            // Hash the password
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser.save().then(user => {


                        var transport = nodemailer.createTransport({
                            serv: 'Gmail',
                            port: 587,
                            auth: {
                                user: 'ahmed.quitzon62@ethereal.email',
                                pass: 'dgyNVfJSBDxagrGRcE'
                            }
                        });

                        var mailOptions = {
                            from: '"AgendEasy" <info@agendeasy.com>',
                            to: email,
                            subject: 'Bienvenue sur Agendeasy',
                            text: 'Votre inscription sur Agend\'Easy a bien été prise en compte. Vous pouvez vous connecter. ',
                            html: '<b>Hey there! </b><br> This is our first message sent with Nodemailer<br /><img src="cid:uniq-mailtrap.png" alt="mailtrap" />'
                        };

                        transport.sendMail(mailOptions, (error, info) => {
                            if (error) {
                                return console.log(error);
                            }
                            console.log('Message sent: %s', info.messageId);
                        });

                        return res.status(201).json({
                            success: true,
                            msg: "L'utilisateur a été créée avec succès."
                        });
                    });
                });
            });
        }
    });
});

/**
 * @route POST api/users/login
 * @desc Signing in the User
 * @access Public
 */
router.post('/login', (req, res) => {
    User.findOne({
        email: req.body.email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        // If there is user we are now going to compare the password
        bcrypt.compare(req.body.password, user.password).then(isMatch => {
            if (isMatch) {
                // User's password is correct and we need to send the JSON Token for that user
                const payload = {
                    _id: user._id,
                    email: user.email,
                    admin: user.admin,
                    company: user.company,
                    job: user.job,
                    name: user.name,
                    surname: user.surname,
                    invite: user.invite
                };
                jwt.sign(payload, key, {
                    expiresIn: 604800
                }, (err, token) => {
                    res.status(200).json({
                        success: true,
                        token: `${token}`,
                        user: user,
                        msg: "Connexion réussie"
                    });
                })
            } else {
                return res.status(500).json({
                    msg: "Mot de passe incorrect",
                    success: false
                });
            }
        })
    });
});


/**
 * @route POST api/users/changePassword
 * @desc Change user's password
 * @access Public
 */
router.post('/changePassword', (req, res) => {
    let {
        admin,
        email,
        company,
        job,
        name,
        surname,
        password,
        newpassword,
        confirm_newpassword,
    } = req.body;
    console.log(req.body)
    User.findOne({
        email: req.body.email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        // If there is user we are now going to compare the password
        bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {

                if (newpassword !== confirm_newpassword) {
                    return res.status(500).json({
                        msg: "Les mots de passe ne correspondent pas"
                    });
                } else {
                    user.password = newpassword
                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(newpassword, salt, (err, hash) => {
                            if (err) throw err;
                            user.password = hash;
                            user.save().then(user => {
                                return res.status(201).json({
                                    success: true,
                                    msg: "Le mot de passe a été modifiée avec succès."
                                });
                            });
                        });
                    });
                }
            } else {
                return res.status(500).json({
                    msg: "Mot de passe incorrect",
                    success: false
                });
            }
        })
    });
});

/**
 * @route POST api/users/deleteUser
 * @desc Delete userdele
 * @access Public
 */
router.post('/deleteUser', (req, res) => {
    let {
        email,
    } = req.body;

    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        try {
            User.deleteOne({email: email}).then(user => {
                return res.status(201).json({
                    success: true,
                    msg: "L'utilisateur a été supprimée avec succès."
                });
            });
        } catch (e) {
            return res.status(500).send("Erreur lors de la suppression");
        }

    })
});


/**
 * @route POST api/users/deleteUser
 * @desc Delete userdele
 * @access Public
 */
router.post('/deleteUserById', (req, res) => {
    let {
        id,
    } = req.body;
    console.log(req.body)
    User.findOne({
        _id: id
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        try {
            User.deleteOne({_id: id}).then(user => {
                return res.status(201).json({
                    success: true,
                    msg: "L'utilisateur a été supprimée avec succès."
                });
            });
        } catch (e) {
            return res.status(500).send("Erreur lors de la suppression");
        }

    })
});

/**
 * @route POST api/users/addFavorite
 * @desc Add favorite
 * @access Public
 */
router.post('/addFavorite', (req, res) => {
    let {
        email,
        salle,
    } = req.body;

    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        Salle.findOne({
            libelle: salle.libelle
        }).then(salle => {
            if (salle) {
                user.favorites.push(salle);
                user.save().then(user => {
                    return res.status(201).json({
                        success: true,
                        msg: "Favori ajouté"
                    });
                })
            } else {
                return res.status(500).send("Aucune salle avec ce libellé");
            }
        })
    })
});

/**
 * @route GET api/users/favorites
 * @desc Return the User's favorites
 * @access Public
 */
router.get('/getFavorites', (req, res) => {
    let {
        email
    } = req.body;
    User.findOne({
        email: email
    }).then(user => {
        if (user) {
            return res.status(200).json(user.favorites);
        } else {
            return res.status(500).send("User inexistant");
        }
    });
});

/**
 * @route POST api/users/profile
 * @desc Return the User's Data
 * @access Private
 */
router.get('/profile', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    return res.json({
        user: req.user
    });
});

/**
 * @route GET api/users/getAllUsers
 * @desc Return an array of all Users
 * @access Public
 */
router.get('/getAllUsers', function (req, res) {
    User.find({}, function (err, users) {
        var userMap = [];

        users.forEach(function (user) {
            userMap.push(user)
        });

        res.send(userMap);
    });
});

/**
 * @route POST api/users/addEvent
 * @desc Add Event
 * @access Public
 */
router.post('/addEventByUser', (req, res) => {
    let {
        title,
        start,
        end,
        description,
        id_reunion,
        salle,
        usersEmail
    } = req.body;

    usersEmail.forEach(emailFromList => {
        User.findOne({
            email: emailFromList
        }).then(user => {
            if (!user) {
                return res.status(500).json({
                    msg: "Cet utilisateur n'existe pas",
                    success: false
                });
            } else {
                let newEvent = ({
                    title,
                    start,
                    end,
                    description,
                    id_reunion,
                    salle,
                    usersEmail
                });
                user.events.push(newEvent);

                user.save().then(user => {
                    if (user) {
                    } else {
                        return res.status(500).json({
                            success: true,
                            msg: "Une erreur s'est produit"
                        });
                    }
                });
            }
        })
    });
    return res.status(202).json({
        success: true,
        msg: "L'évenement a été créée avec succès."
    });
});

/**
 * @route GET api/users/getAllEvents
 * @desc Return an array of all Users
 * @access Public
 */
router.get('/getAllEventsByUser', function (req, res) {
    let email = req.query.email;

    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        } else {
            let eventsMap = [];
            user.events.forEach(function (event) {
                eventsMap.push(event)
            });
            res.send(eventsMap)
        }
    });
});

/**
 * @route POST api/users/deleteEventByUser
 * @desc Delete Event by user
 * @access Public
 */
router.post('/deleteEvent', (req, res) => {
    let {
        id_reunion,
        email
    } = req.body;

    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        }
        try {
            user.events.forEach(event => {
                if (event.id_reunion === id_reunion) {
                    user.events.remove(event)
                }
            });
        } catch (e) {
            return res.status(500).send("Erreur lors de la suppression");
        }
        user.save().then(user => {
            if (user) {
                return res.status(202).json({
                    success: true,
                    msg: "L'évenement a été supprimé avec succès."
                });
            } else {
                return res.status(500).json({
                    success: true,
                    msg: "Une erreur s'est produit"
                });
            }
        });
    })
});

/**
 * @route GET api/users/getAllEvents
 * @desc Return an array of all Users
 * @access Public
 */
router.get('/:id', function (req, res) {
    let email = req.query.email;

    User.findById(req.params.id).then(user => {
        if (!user) {
            return res.status(500).json({
                msg: "Cet utilisateur n'existe pas",
                success: false
            });
        } else {
            res.send(user)
        }
    });
});


module.exports = router;