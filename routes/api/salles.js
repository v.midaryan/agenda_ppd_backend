const express = require('express');
const router = express.Router();
const Salle = require('../../model/Salle');
const moment = require('moment');

var ObjectId = require('mongoose').Types.ObjectId;

/**
 * @route POST api/salles/addMeetingRoom
 * @desc Create Salle
 * @access Public
 */
router.post('/addMeetingRoom', (req, res) => {
    let {
        libelle,
        capacite,
        emplacement,
        pmr,
        equipement,
    } = req.body;

    Salle.findOne({
        libelle: libelle
    }).then(salle => {
        if (salle) {
            return res.status(500).send("cette salle est déjà enregistrée !");
        }
        else {
            let newSalle = new Salle({
                libelle,
                capacite,
                emplacement,
                pmr,
                equipement
            });

            newSalle.save().then(salle => {
                return res.status(201).json({
                    success: true,
                    msg: "La salle a été créée avec succès."
                });
            });
        }
    })

});

router.post('/editMeetingRoom', (req, res) => {
    let {
        libelle,
        newlibelle,
        capacite,
        emplacement,
        pmr,
        equipement,
    } = req.body;
    Salle.findOne({
        libelle: libelle
    }).then(salle => {
        if (salle) {
            salle.libelle = newlibelle,
            salle.capacite = capacite,
            salle.emplacement = emplacement,
            salle.pmr = pmr,
            salle.equipement = equipement;

            salle.save().then(salle => {
                return res.status(201).json({
                    success: true,
                    msg: "La salle a été modifiée avec succès."
                });
            })
        }
        else {
            return res.status(500).send("cette salle est inexistante !");
        }
    })

});

router.post('/deleteMeetingRoom', (req, res) => {
    let {
        libelle
    } = req.body;

    try {
        Salle.deleteOne( { libelle: libelle} ).then (salle => {
            return res.status(201).json({
                success: true,
                msg: "La salle a été supprimée avec succès."
            });
        });
     } catch (e) {
        return res.status(500).send("Erreur lors de la suppression");
     }
});

router.get('/getMeetingRoomByLibelle', (req, res) => {
    let libelle= req.query.libelle;

    Salle.findOne({
        libelle: libelle
    }).then(salle => {
        if (salle) {
            return res.status(200).json(salle);
        } else {
            return res.status(500).send("Aucune salle avec ce libellé");
        }
    });
});

router.get('/getMeetingRoomById', (req, res) => {
    let id=req.query.id;

    Salle.findById(new ObjectId(id)).then(salle => {
        if (salle) {
            return res.status(200).json(salle);
        } else {
            return res.status(500).send("Aucune salle avec cet identifiant");
        }
    });
});

router.put('/updateMeetingRoomByLibelle', (req, res) => {
    let libelle= req.query.libelle;

    Salle.findOneAndUpdate(libelle, req.body).then(salle => {
        if (salle) {
            return res.status(200).json("La salle a été modifiée avec succès.");
        } else {
            return res.status(500).send("Cette salle n'existe pas !");
        }
    });
});

router.delete('/deleteMeetingRoomByLibelle', (req, res) => {
    let libelle= req.query.libelle;

    Salle.deleteOne({
        libelle: libelle
    }, function (err, result) {
        if (result.deletedCount < 1) {
            return res.status(500).send("Cette salle n'existe pas !");
        } else {
            return res.status(200).json("La salle a été supprimé avec succès.");
        }
    });
});

router.get('/getAllMeetingRoom', function(req, res) {
    Salle.find({}, function(err, salles) {
        var salleMap = [];

        salles.forEach(function(salle) {
            salleMap.push(salle)
        });

        res.send(salleMap);
    });
});

router.post('/deleteReservation', function (req, res) {
    let {
        libelle,
        date_debut,
        date_fin
    } = req.body;

    Salle.findOne({
        libelle: libelle
    }).then(salle => {
        salle.reservations.forEach(reservation => {
            if(moment(reservation.date_debut).isSame(date_debut) && (moment(reservation.date_fin).isSame(date_fin))) {
                salle.reservations.remove(reservation)
            }
        });
        salle.save().then(salle => {
            if (salle) {
                return res.status(200).json("La réservation a été supprimé avec succès.");
            } else {
                return res.status(500).send("Une erreur s'est produit");
            }
        });
    });

});

module.exports = router;
