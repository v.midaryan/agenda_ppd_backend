const express = require('express');
const router = express.Router();
const Reunion = require('../../model/Reunion');
const Salle = require('../../model/Salle');
const moment = require('moment');

var ObjectId = require('mongoose').Types.ObjectId;

/**
 * @route POST api/reunions/addReunion
 * @desc Create Salle
 * @access Public
 */
router.post('/createMeeting', (req, res) => {
    let {
        titre,
        date_debut,
        date_fin,
        salle,
        users,
    } = req.body;

    Salle.findOne({
        libelle: salle.libelle
    }).then(salle => {
        let compareDate;
        let startDate = moment(date_debut).format();
        let endDate = moment(date_fin).format();
        salle.reservations.forEach(reservation => {
            compareDate = moment(reservation.date_debut).format();
            if((moment(compareDate).isAfter(startDate) || moment(compareDate).isSame(startDate)) &&
                (moment(compareDate).isBefore(endDate) || moment(compareDate).isSame(endDate))) {
                return res.status(500).send("La salle possède déja une réunion à cette heure !");
                throw DOMException;
            }
        });
        let newReunion= new Reunion({
            titre,
            date_debut,
            date_fin,
            salle,
            users,
        });

        salle.reservations.push({date_debut, date_fin});

        salle.save();

        newReunion.save().then(reunion => {
            return res.status(202).json({
                success: true,
                msg: "La réunion a été créée avec succès.",
                id_reunion: reunion._id
            });
        });
    })
});

module.exports = router;