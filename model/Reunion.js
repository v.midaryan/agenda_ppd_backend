const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Equipement Schema
const EquipementSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
});

// Create Salle Schema
const SalleSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    capacite: {
        type: Number,
        required: true
    },
    emplacement: {
        type: String,
        required: true
    },
    pmr: {
        type: Boolean,
        required: true
    },
    equipement: {
        type: [EquipementSchema],
        required: true
    },
    reservations: [
        {
            date_debut: Date,
            date_fin: Date,
        }
    ],
});

// Create the User Schema
const UserSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        required: true
    },
    company: {
        type: String
    },
    job: {
        type: String
    },
    name: {
        type: String
    },
    surname: {
        type: String
    },
    invite: {
        type: Boolean
    },
    date: {
        type: Date,
        default: Date.now
    },
    favorites: [
        {type: Schema.ObjectId, ref: 'Salle'}
    ]
});

// Create the Reunion Schema
const ReunionSchema = new Schema({
    titre: {
        type: String,
        required: true
    },
    date_debut: {
        type: Date,
        required: true
    },
    date_fin: {
        type: Date,
        required: true
    },
    salle: {
        type: SalleSchema,
        ref: 'Salle',
        required: true,
    },
    users: [
        {type: UserSchema, ref: 'User'}
    ],
});

module.exports = Reunion = mongoose.model('reunions', ReunionSchema);