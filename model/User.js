const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Equipement Schema
const EquipementSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
});

// Create Salle Schema
const SalleSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    capacite: {
        type: Number,
        required: true
    },
    emplacement: {
        type: String,
        required: true
    },
    pmr: {
        type: Boolean,
        required: true
    },
    equipement: {
        type: [EquipementSchema],
        required: true
    },
    reservations: [
        {
            date_debut: Date,
            date_fin: Date,
        }
    ],
});

// Create Event Schema
const EventSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    start: {
        type: Date,
        required: true
    },
    end: {
        type: Date,
        required: true
    },
    description: {
        type: String
    },
    id_reunion: {
        type: String,
        required: true
    },
    salle: {
        type: SalleSchema,
        required: true
    },
    usersEmail: {
        type: [String],
        required: true
    }
});

// Create the User Schema
const UserSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        required: true
    },
    company: {
        type: String
    },
    job: {
        type: String
    },
    name: {
        type: String
    },
    surname: {
        type: String
    },
    invite: {
        type: Boolean
    },
    date: {
        type: Date,
        default: Date.now
    },
    favorites: [
        {type: Schema.ObjectId, ref: 'Salle'}
    ],
    events: [
        {type: EventSchema, required: true}
    ]
});

module.exports = User = mongoose.model('users', UserSchema);