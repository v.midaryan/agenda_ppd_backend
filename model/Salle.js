const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Equipement Schema
const EquipementSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
});

// Create Salle Schema
const SalleSchema = new Schema({
    libelle: {
        type: String,
        required: true
    },
    capacite: {
        type: Number,
        required: true
    },
    emplacement: {
        type: String,
        required: true
    },
    pmr: {
        type: Boolean,
        required: true
    },
    equipement: {
        type: [EquipementSchema],
        required: true
    },
    reservations: [
        {
            date_debut: Date,
            date_fin: Date,
        }
    ],
});

module.exports = Salle = mongoose.model('salles', SalleSchema);